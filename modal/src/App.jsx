import React from 'react'
import Modal from './components/modal/modal'
import Button from './components/button/button'
import { modals } from './components/modal/utils'
import './App.scss'

export default class App extends React.Component {
	state = {
		activeModal: null,
	}

	toggleModal(obj = null) {
		this.setState({
			activeModal: obj,
		})
	}

	render() {
		const modal = this.state.activeModal
		return (
			<div className='app'>
				<Button
					type='danger'
					onClick={() => {
						this.toggleModal(modals.deleteFile)
					}}
				>
					First Modal
				</Button>
				<Button type='primary' onClick={() => this.toggleModal(modals.addFile)}>
					Second Modal
				</Button>

				{modal && (
					<Modal
						isWrapperClose={modal.isWrapperClose}
						type={modal.type}
						onClose={() => this.toggleModal()}
						title='Do you want to delete this file?'
						actions={
							modal.actions &&
							(() =>
								modal.actions({
									confirm: () => {
										console.log('hello world')
									},
									close: () => this.toggleModal(),
								}))
						}
						body={modal.body}
					></Modal>
				)}
			</div>
		)
	}
}
