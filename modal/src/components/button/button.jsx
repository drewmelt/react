import React from 'react'
import './button.scss'

class Button extends React.Component {
	type = {
		default: 'btn--default',
		danger: 'btn--danger',
		primary: 'btn--primary',
	}

	render() {
		const typeStyle = this.type[this.props.type] || ''
		const btnStyle = this.props.className || ''
		const style = `btn ${btnStyle} ${typeStyle}`.trim()
		return (
			<button className={style} onClick={this.props.onClick}>
				{this.props.children}
			</button>
		)
	}
}

export default Button
