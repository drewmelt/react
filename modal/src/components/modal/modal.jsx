import React from 'react'
import Button from '../button/button'
import { VscChromeClose } from 'react-icons/vsc'
import './modal.scss'

export default class Modal extends React.Component {
	onClose = () => this.props.onClose()

	type = {
		default: {
			modalWrapper: 'modal-wrapper',
			modal: 'modal',
			modalContent: 'modal__content',
			modalHeader: 'modal__header',
			modalTitle: 'modal__title',
			modalBody: 'modal__body',
			modalFooter: 'modal__footer',
		},
		danger: {
			modalWrapper: 'modal-wrapper',
			modal: 'modal text--light',
			modalContent: 'modal__content bg--danger',
			modalHeader: 'modal__header bg--danger-dark',
			modalTitle: 'modal__title',
			modalBody: 'modal__body',
			modalFooter: 'modal__footer',
		},
		primary: {
			modalWrapper: 'modal-wrapper',
			modal: 'modal',
			modalContent: 'modal__content',
			modalHeader: 'modal__header bg--primary text--light',
			modalTitle: 'modal__title',
			modalBody: 'modal__body',
			modalFooter: 'modal__footer',
		},
	}

	render() {
		const btnType = this.type[this.props.type] || this.type.default
		return (
			<div
				className={btnType.modalWrapper}
				onClick={this.props.isWrapperClose ? this.onClose : () => {}}
			>
				<div className={btnType.modal} onClick={e => e.stopPropagation()}>
					<div className={btnType.modalContent}>
						<div className={btnType.modalHeader}>
							<h4 className={btnType.modalTitle}>{this.props.title}</h4>
							<Button className={'modal__icon'} onClick={this.onClose}>
								<VscChromeClose />
							</Button>
						</div>
						<div className={btnType.modalBody}>{this.props.body}</div>
						{this.props.actions && (
							<div className={btnType.modalFooter}>{this.props.actions()}</div>
						)}
					</div>
				</div>
			</div>
		)
	}
}
