import Button from '../button/button'

export const modals = {
	deleteFile: {
		isWrapperClose: true,
		type: 'danger',
		title: 'Do you want to delete this file?',
		body: (
			<>
				<p className='text text--center'>
					Once you delete this file, it won’t be possible to undo this action.
				</p>
				<p className='text text--center'>Are you sure you want to delete it?</p>
			</>
		),
		actions: function ({ close }) {
			return (
				<div className='center'>
					<Button type='danger'>Ok</Button>
					<Button type='danger' onClick={close}>
						Cancel
					</Button>
				</div>
			)
		},
	},
	addFile: {
		isWrapperClose: false,
		type: 'primary',
		title: 'Do you want to delete this file?',
		body: (
			<>
				<p className='text text--center'>
					Once you delete this file, it won’t be possible to undo this action.
				</p>
				<p className='text text--center'>Are you sure you want to delete it?</p>
			</>
		),
		actions: function ({ close }) {
			return (
				<div className='center'>
					<Button type='primary'>Ok</Button>
					<Button type='primary' onClick={close}>
						Cancel
					</Button>
				</div>
			)
		},
	},
}
