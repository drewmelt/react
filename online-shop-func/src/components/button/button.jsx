import React from 'react'
import './button.scss'

const btmType = {
	default: 'btn--default',
	danger: 'btn--danger',
	primary: 'btn--primary',
}

const Button = ({ type, onClick, children, className }) => {
	const typeStyle = btmType[type] || ''
	const btnStyle = className || ''
	const style = `btn ${btnStyle} ${typeStyle}`.trim()
	return (
		<button className={style} onClick={onClick}>
			{children}
		</button>
	)
}

export default Button
