import React from 'react'
import Button from '../button/button'
import './favoriteItem.scss'

const FavoriteItem = ({ title, image, openModal }) => {
	return (
		<div className='favorite__item'>
			<div className='favorite__image-wrapper'>
				<img src={image} alt={title} className='favorite__image' />
			</div>
			<div className='favorite__body'>
				<h4 className='favorite__title'>{title}</h4>
			</div>
			<div className='favorite__controller'>
				<Button type={'danger'} onClick={openModal}>
					Remove from Favotire
				</Button>
			</div>
		</div>
	)
}

export default FavoriteItem
