import React from 'react'
import PropTypes from 'prop-types'
import { VscStarEmpty, VscStarFull } from 'react-icons/vsc'
import Button from '../button/button'
import './goodsItem.scss'

const GoodsItem = ({ title, image, openModal, addToFav, isFavourite }) => {
	return (
		<div className='goods__item'>
			<div className='goods__image-wrapper'>
				<img src={image} alt={title} className='goods__image' />
			</div>
			<div className='goods__body'>
				<h4 className='goods__title'>{title}</h4>
			</div>
			<div className='goods__controller'>
				<Button onClick={() => addToFav()}>
					{isFavourite ? <VscStarFull /> : <VscStarEmpty />}
				</Button>
				<Button type={'primary'} onClick={() => openModal()}>
					Buy
				</Button>
			</div>
		</div>
	)
}

export default GoodsItem

GoodsItem.propTypes = {
	isFavourite: PropTypes.bool,
	addToFav: PropTypes.func.isRequired,
	openModal: PropTypes.func.isRequired,
}
