import React from 'react'
import GoodsItem from './goodsItem'
import PropTypes from 'prop-types'
import './goodsList.scss'

const GoodsList = ({ data, favs, openModal, addToFav }) => {
	const isFavourite = item => !!favs.find(card => card.id === item.id)
	return (
		<div className='goods__list'>
			{data.map(good => (
				<GoodsItem
					{...good}
					key={good.id}
					isFavourite={isFavourite(good)}
					addToFav={() => addToFav(good)}
					openModal={() => openModal(good)}
				/>
			))}
		</div>
	)
}

GoodsList.propTypes = {
	data: PropTypes.arrayOf(PropTypes.object).isRequired,
	addToFav: PropTypes.func.isRequired,
	openModal: PropTypes.func.isRequired,
}

export default GoodsList
