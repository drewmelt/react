import React from 'react'
import Button from '../button/button'
import { VscChromeClose } from 'react-icons/vsc'
import './modal.scss'

const modalType = {
	default: {
		modalWrapper: 'modal-wrapper',
		modal: 'modal',
		modalContent: 'modal__content',
		modalHeader: 'modal__header',
		modalTitle: 'modal__title',
		modalBody: 'modal__body',
		modalFooter: 'modal__footer',
	},
	danger: {
		modalWrapper: 'modal-wrapper',
		modal: 'modal text--light',
		modalContent: 'modal__content bg--danger',
		modalHeader: 'modal__header bg--danger-dark',
		modalTitle: 'modal__title',
		modalBody: 'modal__body',
		modalFooter: 'modal__footer',
	},
	primary: {
		modalWrapper: 'modal-wrapper',
		modal: 'modal',
		modalContent: 'modal__content',
		modalHeader: 'modal__header bg--primary text--light',
		modalTitle: 'modal__title',
		modalBody: 'modal__body',
		modalFooter: 'modal__footer',
	},
}

const Modal = ({ type, isWrapperClose, onClose, title, body, actions }) => {
	const btnType = modalType[type] || modalType.default
	return (
		<div
			className={btnType.modalWrapper}
			onClick={isWrapperClose ? onClose : () => {}}
		>
			<div className={btnType.modal} onClick={e => e.stopPropagation()}>
				<div className={btnType.modalContent}>
					<div className={btnType.modalHeader}>
						<h4 className={btnType.modalTitle}>{title}</h4>
						<Button className={'modal__icon'} onClick={onClose}>
							<VscChromeClose />
						</Button>
					</div>
					<div className={btnType.modalBody}>{body}</div>
					{actions && <div className={btnType.modalFooter}>{actions()}</div>}
				</div>
			</div>
		</div>
	)
}

export default Modal
