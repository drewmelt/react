import React from 'react'
import Container from '../container/container'
import { NavLink } from 'react-router-dom'
import utils from './utils'
import './navbar.scss'

const NavBar = () => {
	return (
		<nav className='navbar'>
			<Container>
				<ul className='navbar__list'>
					{utils.map((link, ind) => {
						return (
							<li key={ind} className={'navbar__item'}>
								<NavLink
									exact
									to={link.url}
									className='navbar__link'
									activeClassName='navbar__link--active'
								>
									{link.text}
								</NavLink>
							</li>
						)
					})}
				</ul>
			</Container>
		</nav>
	)
}

export default NavBar
