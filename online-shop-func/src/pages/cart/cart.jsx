import React, { useState, useEffect } from 'react'
import CartList from '../../components/cart/cartList'
import Container from '../../components/container/container'
import NavBar from '../../components/navbar/navbar'
import Modal from '../../components/modal/modal'
import utils from '../../components/modal/utils'

const CartPage = () => {
	const [cart, setCart] = useState([])
	const [card, setCard] = useState(null)
	const [modal, setModal] = useState(null)

	useEffect(() => {
		const cart = JSON.parse(localStorage.getItem('cart')) || []
		setCart(cart)
	}, [])

	const removeFromCart = card => {
		const index = cart.findIndex(item => item.id === card.id)
		cart.splice(index, 1)
		localStorage.setItem('cart', JSON.stringify([...cart]))
	}

	const toggleModal = (obj = null) => setModal(obj)

	return (
		<>
			<NavBar />
			<Container>
				<CartList
					cart={cart}
					openModal={data => {
						setCard(data)
						toggleModal(utils.removeFromCart)
					}}
				/>
			</Container>
			{modal && (
				<Modal
					{...modal}
					onClose={() => toggleModal()}
					actions={
						modal.actions &&
						(() =>
							modal.actions({
								close: () => toggleModal(),
								ok: () => {
									removeFromCart(card)
									toggleModal()
								},
							}))
					}
				></Modal>
			)}
		</>
	)
}

export default CartPage
