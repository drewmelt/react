import React, { useState, useEffect } from 'react'
import FavoriteList from '../../components/favorite/favoriteList'
import Container from '../../components/container/container'
import NavBar from '../../components/navbar/navbar'
import Modal from '../../components/modal/modal'
import utils from '../../components/modal/utils'

const FavouritePage = () => {
	const [favs, setFavs] = useState([])
	const [card, setCard] = useState(null)
	const [modal, setModal] = useState(null)

	useEffect(() => {
		const favs = JSON.parse(localStorage.getItem('favs')) || []
		setFavs(favs)
	}, [])

	const removeFromFavorite = card => {
		const newFav = favs.filter(item => item.id !== card.id)
		setFavs(newFav)
		localStorage.setItem('favs', JSON.stringify(newFav))
	}

	const toggleModal = (obj = null) => setModal(obj)

	return (
		<>
			<NavBar />
			<Container>
				<FavoriteList
					favs={favs}
					openModal={data => {
						setCard(data)
						toggleModal(utils.removeFromFavorite)
					}}
				/>
			</Container>
			{modal && (
				<Modal
					{...modal}
					onClose={() => toggleModal()}
					actions={
						modal.actions &&
						(() =>
							modal.actions({
								close: () => toggleModal(),
								ok: () => {
									removeFromFavorite(card)
									toggleModal()
								},
							}))
					}
				></Modal>
			)}
		</>
	)
}

export default FavouritePage
