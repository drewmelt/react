import React from 'react'

import { Switch, Route } from 'react-router-dom'
import HomePage from '../pages/home/home'
import GoodsPage from '../pages/goods/goods'
import FavoritePage from '../pages/favorite/favorite'
import CartPage from '../pages/cart/cart'

const AppRouter = () => {
	return (
		<Switch>
			<Route path={'/'} exact>
				<HomePage />
			</Route>
			<Route path={'/goods'}>
				<GoodsPage />
			</Route>
			<Route path={'/favorite'}>
				<FavoritePage />
			</Route>
			<Route path={'/cart'}>
				<CartPage />
			</Route>
		</Switch>
	)
}

export default AppRouter
