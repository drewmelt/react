import './App.scss'
import React from 'react'
import AppRouter from './router/appRouter'
import { BrowserRouter as Router } from 'react-router-dom'

const App = () => {
	return (
		<Router>
			<AppRouter />
		</Router>
	)
}

export default App
