import React from 'react'
import Button from '../button/button'
import './cartItem.scss'

const CartItem = ({ title, image, openModal }) => {
	return (
		<div className='cart__item'>
			<div className='cart__image-wrapper'>
				<img src={image} alt={title} className='cart__image' />
			</div>
			<div className='cart__body'>
				<h4 className='cart__title'>{title}</h4>
			</div>
			<div className='cart__controller'>
				<Button type={'danger'} onClick={openModal}>
					Remove from Cart
				</Button>
			</div>
		</div>
	)
}

export default CartItem
