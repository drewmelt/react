import React from 'react'
import CartItem from './cartItem'
import './cartList.scss'

const CartList = ({ cart, openModal }) => {
	return (
		<>
			{cart.length ? (
				<div className='cart__list'>
					{cart.map((item, index) => (
						<CartItem {...item} key={index} openModal={() => openModal(item)} />
					))}
				</div>
			) : (
				<p>Empty</p>
			)}
		</>
	)
}

export default CartList
