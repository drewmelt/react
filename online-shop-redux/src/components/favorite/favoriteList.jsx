import React from 'react'
import FavoriteItem from './favoriteItem'
import './favoriteList.scss'

const FavoriteList = ({ favs, openModal }) => {
	return (
		<>
			{favs.length ? (
				<div className='favorite__list'>
					{favs.map(item => (
						<FavoriteItem
							{...item}
							key={item.id}
							openModal={() => openModal(item)}
						/>
					))}
				</div>
			) : (
				<p>Empty</p>
			)}
		</>
	)
}

export default FavoriteList
