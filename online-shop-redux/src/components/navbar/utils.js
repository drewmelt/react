const utils = [
	{
		url: '/',
		text: 'home',
	},
	{
		url: '/goods',
		text: 'goods',
	},
	{
		url: '/favorite',
		text: 'favorite',
	},
	{
		url: '/cart',
		text: 'cart',
	},
]

export default utils
