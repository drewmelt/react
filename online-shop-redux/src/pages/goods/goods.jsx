import React, { useState, useEffect } from 'react'
import GoodsList from '../../components/goods/goodsList'
import Container from '../../components/container/container'
import NavBar from '../../components/navbar/navbar'
import Modal from '../../components/modal/modal'
import utils from '../../components/modal/utils'

const GoodsPage = () => {
	const [favs, setFavs] = useState([])
	const [cart, setCart] = useState([])
	const [data, setData] = useState([])
	const [card, setCard] = useState(null)
	const [modal, setModal] = useState(null)

	useEffect(() => {
		;(async () => {
			const data = await fetch('./data.json').then(data => data.json())
			setData(data)
		})()
		const favs = JSON.parse(localStorage.getItem('favs')) || []
		const cart = JSON.parse(localStorage.getItem('cart')) || []

		setFavs(favs)
		setCart(cart)
	}, [])

	const saveCartToFavorite = card => {
		const isFav = favs.find(item => item.id === card.id)
		const favArr = !isFav
			? [...favs, card]
			: favs.filter(item => item.id !== card.id)

		setFavs(favArr)
		localStorage.setItem('favs', JSON.stringify(favArr))
	}

	const saveCartToLocal = item => {
		const cartArr = [...cart, item]
		setCart(cartArr)
		localStorage.setItem('cart', JSON.stringify(cartArr))
	}

	const toggleModal = (obj = null) => setModal(obj)

	return (
		<>
			<NavBar />
			<Container>
				<GoodsList
					favs={favs}
					data={data}
					addToFav={card => saveCartToFavorite(card)}
					openModal={data => {
						setCard(data)
						toggleModal(utils.addToCart)
					}}
				/>
			</Container>
			{modal && (
				<Modal
					{...modal}
					onClose={() => toggleModal()}
					actions={
						modal.actions &&
						(() =>
							modal.actions({
								close: () => toggleModal(),
								ok: () => {
									saveCartToLocal(card)
									toggleModal()
								},
							}))
					}
				></Modal>
			)}
		</>
	)
}

export default GoodsPage
