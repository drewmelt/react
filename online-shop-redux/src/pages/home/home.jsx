import React from 'react'

import NavBar from '../../components/navbar/navbar'

const HomePage = () => {
	return (
		<>
			<NavBar />
			<h1>Home Page</h1>
		</>
	)
}

export default HomePage
