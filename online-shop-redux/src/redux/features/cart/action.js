import { types } from 'types'

const getAllEmailsAction = () => ({
	type: types.GET_ALL_CART,
})

const setAllCartAction = payload => ({
	type: types.EMAILS_SET,
	payload,
})

const setAllCart = () => (dispatch, getState) => {
	const data = JSON.parse(localStorage.getItem('cart'))
	dispatch(setAllCartAction(data))
}

const actions = {
	getAllEmailsAction,
	setAllCart,
}

export default actions
