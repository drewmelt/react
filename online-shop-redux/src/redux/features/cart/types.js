const ADD_TO_CART = 'ADD_TO_CART'
const GET_ALL_CART = 'GET_ALL_CART'
const SET_ALL_CART = 'SET_ALL_CART'

const types = {
	ADD_TO_CART,
	GET_ALL_CART,
	SET_ALL_CART,
}

export default types
