import './App.scss'
import React from 'react'
import AppRouter from './router/appRouter'
import { BrowserRouter as Router } from 'react-router-dom'

export default class App extends React.Component {
	render() {
		return (
			<Router>
				<AppRouter />
			</Router>
		)
	}
}
