import React from 'react'
import PropTypes from 'prop-types'
import './goodsItem.scss'
import { VscStarEmpty, VscStarFull } from 'react-icons/vsc'
import Button from '../button/button'

export default class GoodsItem extends React.Component {
	render() {
		const { title, image, openModal, addToFav, isFavourite } = this.props
		return (
			<div className='goods__item'>
				<h4 className='goods__title'>{title}</h4>
				<div className='goods__image-wrapper'>
					<img src={image} alt={title} className='goods__image' />
				</div>
				<Button onClick={() => addToFav()}>
					{isFavourite ? <VscStarFull /> : <VscStarEmpty />}
				</Button>
				<Button type={'primary'} onClick={() => openModal()}>
					Add to Cart
				</Button>
			</div>
		)
	}
}
GoodsItem.propTypes = {
	isFavourite: PropTypes.bool,
	addToFav: PropTypes.func.isRequired,
	openModal: PropTypes.func.isRequired,
}
