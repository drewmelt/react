import React from 'react'
import GoodsItem from './goodsItem'
import PropTypes from 'prop-types'
import './goodsList.scss'

export default class GoodsList extends React.Component {
	render() {
		const favs = JSON.parse(localStorage.getItem('favs')) || []
		const isFavourite = item =>
			favs.findIndex(card => card.id === item.id) === -1 ? false : true
		const { data, openModal, addToFav } = this.props
		return (
			<div className='goods__list'>
				{data.map(good => (
					<GoodsItem
						{...good}
						isFavourite={isFavourite(good)}
						key={good.id}
						addToFav={() => addToFav(good)}
						openModal={() => openModal(good)}
					/>
				))}
			</div>
		)
	}
}

GoodsList.propTypes = {
	data: PropTypes.arrayOf(PropTypes.object).isRequired,
	modal: PropTypes.object.isRequired,
	addToFav: PropTypes.func.isRequired,
	openModal: PropTypes.func.isRequired,
}
