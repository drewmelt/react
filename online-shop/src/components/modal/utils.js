import Button from '../button/button'

const utils = {
	addToCart: {
		isWrapperClose: true,
		type: 'primary',
		title: 'Add to Cart?',
		body: (
			<p className='text text--center'>
				Do you want to add this card to the Card.
			</p>
		),
		actions: function ({ close, ok }) {
			return (
				<div className='center'>
					<Button type='primary' onClick={ok}>
						Ok
					</Button>
					<Button type='primary' onClick={close}>
						Cancel
					</Button>
				</div>
			)
		},
	},
	deleteFile: {
		isWrapperClose: true,
		type: 'danger',
		title: 'Do you want to delete this file?',
		body: (
			<>
				<p className='text text--center'>
					Once you delete this file, it won’t be possible to undo this action.
				</p>
				<p className='text text--center'>Are you sure you want to delete it?</p>
			</>
		),
		actions: function ({ close }) {
			return (
				<div className='center'>
					<Button type='danger'>Ok</Button>
					<Button type='danger' onClick={close}>
						Cancel
					</Button>
				</div>
			)
		},
	},
	addFile: {
		isWrapperClose: false,
		type: 'primary',
		title: 'Do you want to add this item into the cart?',
		body: (
			<>
				<p className='text text--center'>
					This item will be added to your cart
				</p>
			</>
		),
		actions: function ({ close }) {
			return (
				<div className='center'>
					<Button type='primary'>Ok</Button>
					<Button type='primary' onClick={close}>
						Cancel
					</Button>
				</div>
			)
		},
	},
}

export default utils
