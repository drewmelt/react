import React from 'react'
import Container from '../container/container'
import { Link } from 'react-router-dom'
import utils from './utils'
import './navbar.scss'

export default class NavBar extends React.Component {
	render() {
		return (
			<nav className='navbar'>
				<Container>
					<ul className='navbar__list'>
						{utils.map((link, ind) => {
							return (
								<li
									key={ind}
									className={
										this.props.activeLink === link.url
											? 'navbar__item active'
											: 'navbar__item'
									}
								>
									<Link to={link.url} className='navbar__link'>
										{link.text}
									</Link>
								</li>
							)
						})}
					</ul>
				</Container>
			</nav>
		)
	}
}
