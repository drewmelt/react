import React from 'react'
import GoodsList from '../../components/goods/goodsList'
import Container from '../../components/container/container'
import NavBar from '../../components/navbar/navbar'
import Modal from '../../components/modal/modal'
import utils from '../../components/modal/utils'

export default class GoodsPage extends React.Component {
	state = {
		favs: null,
		card: null,
		data: null,
		activeModal: null,
	}

	saveCartToFavorite(card) {
		const favs = JSON.parse(localStorage.getItem('favs')) || []
		const findFav = favs.findIndex(item => item.id === card.id)
		if (findFav === -1) {
			favs.push(card)
		} else {
			favs.splice(findFav, 1)
		}
		localStorage.setItem('favs', JSON.stringify(favs))
		this.setState({
			favs,
		})
	}

	saveCartToLocal(item) {
		const cart = JSON.parse(localStorage.getItem('cart')) || []
		localStorage.setItem('cart', JSON.stringify([...cart, item]))
	}

	toggleModal(obj = null) {
		this.setState({
			activeModal: obj,
		})
	}

	async componentDidMount() {
		const data = await fetch('./data.json').then(data => data.json())
		localStorage.setItem('data', JSON.stringify(data))
		this.setState({
			data: data,
		})
	}

	render() {
		const modal = this.state.activeModal
		return (
			<>
				<NavBar activeLink={this.props.activeLink} />
				<Container>
					<GoodsList
						data={this.state.data || []}
						modal={utils.addToCart}
						addToFav={card => this.saveCartToFavorite(card)}
						openModal={data => {
							this.setState({
								card: data,
							})
							this.toggleModal(utils.addToCart)
						}}
					/>
				</Container>
				{modal && (
					<Modal
						{...modal}
						onClose={() => this.toggleModal()}
						actions={
							modal.actions &&
							(() =>
								modal.actions({
									close: () => this.toggleModal(),
									ok: () => {
										this.saveCartToLocal(this.state.card)
										this.toggleModal()
									},
								}))
						}
					></Modal>
				)}
			</>
		)
	}
}
