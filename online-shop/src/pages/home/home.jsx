import React from 'react'

import NavBar from '../../components/navbar/navbar'
export default class HomePage extends React.Component {
	render() {
		return <NavBar activeLink={this.props.activeLink} />
	}
}
