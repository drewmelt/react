import React from 'react'

import { Switch, Route } from 'react-router-dom'
import HomePage from '../pages/home/home'
import GoodsPage from '../pages/goods/goods'

export default class AppRouter extends React.Component {
	render() {
		return (
			<Switch>
				<Route path={'/'} exact>
					<HomePage activeLink={'/'} />
				</Route>
				<Route path={'/goods'}>
					<GoodsPage activeLink={'/goods'} />
				</Route>
			</Switch>
		)
	}
}
